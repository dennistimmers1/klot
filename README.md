# Klot

Klot is a incredibly lightweight (600 bytes gzip+min) translation module with built in variable interpolation written in Typescript. Meet typed translations. It's just a simple factory function that produces a `translate` function. Translations are supplied using objects or JSON and can either be a simple string or a complex object that can be transformed using transformation function.

## Usage

Find out more in the core package or extensions.

- [Klot](packages/Klot)

## Contributing

TBD
